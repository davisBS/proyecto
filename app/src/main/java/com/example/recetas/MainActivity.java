package com.example.recetas;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
private EditText etBuscar;
private Button btBuscar,btLimpiar;
private TextView tvIngrendientes, tvInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.etBuscar=(EditText)findViewById(R.id.etBuscar);
        this.btBuscar=(Button)findViewById(R.id.btBuscar);
        this.tvIngrendientes=(TextView)findViewById(R.id.tvIngredientes);
        this.tvInfo=(TextView)findViewById(R.id.tvInfo);
        this.btLimpiar=(Button)findViewById(R.id.btLimpiar);


        this.btBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String receta=etBuscar.getText().toString();

                String url="https://api.edamam.com/search?q="+receta+"&app_id=66f7c485&app_key=75392381baccc55f0e255c4384cde1a3&from=0&to=2";
                StringRequest solicitud=new StringRequest(
                        Request.Method.GET,
                        url,
                        new Response.Listener<String>(){
                            @Override
                            public void onResponse(String response){
                                try{
                                    JSONObject respuestaJSON = new JSONObject(response);
                                    ArrayList<String>nombres=new ArrayList<>();
                                    JSONArray recetas=respuestaJSON.getJSONArray("hits");
                                    JSONObject ingredientes=recetas.getJSONObject(0);
                                    JSONObject rece=ingredientes.getJSONObject("recipe");
                                    JSONArray ingred=rece.getJSONArray("ingredients");
                                    for(int i=0;i<ingred.length();i++){
                                        JSONObject lista=ingred.getJSONObject(i);
                                        nombres.add(lista.getString("text")+"\n");
                                        tvIngrendientes.setText("Los ingredientes son: "+"\n"+nombres);
                                    }
                                    ArrayList<String> informacion=new ArrayList<>();
                                    JSONArray comida=rece.getJSONArray("digest");
                                    for(int i=0;i<comida.length();i++){
                                        JSONObject objeto=comida.getJSONObject(i);
                                        informacion.add(objeto.getString("label"));
                                        informacion.add(objeto.getString("total")+"\n");
                                        tvInfo.setText("Información nutricional"+informacion);
                                    }


                                }catch(JSONException e){
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener(){
                            @Override
                            public void onErrorResponse(VolleyError error){
                                //Algo falló
                                Toast.makeText(getApplicationContext(),"Hay un error",Toast.LENGTH_SHORT).show();
                            }
                        }
                );
                RequestQueue listaEspera= Volley.newRequestQueue(getApplicationContext());
                listaEspera.add(solicitud);


            }
        });
        this.btLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvIngrendientes.setText("");
                tvInfo.setText("");
                etBuscar.setText("");
            }
        });




    }
}
